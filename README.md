Careful! This is a work-in-progress and NOT ready-for-production project!

# Pierhost
_A system built on pi-gen to create Raspbian images for the Pier project_

## What is Pierhost?

This project aims to provide users with an easy-to-deploy, lightweight, all-in-one homeserver solution for your Raspberry Pi, including different components, such as a fileserver, git server, database, VPN, password manager, and many more. Simply download the appropriate image provided here, flash it to an SD-Card, boot your Raspberry Pi from it, and finalize the configuration using the web interface available from the local network.

### Services currently provided

* A WireGuard VPN tunnel. On usage, see below "Setting up Wireguard VPN".
* A PostgreSQL database for use by other services. The database can be accessed through /adminer/
* The dashboard for Traefik, the reverse proxy used to route requests to the services. The dashboard can be accessed through /dashboard/
* A GoGS Git Server. The Git server can be accessed through /gogs/
* A Mailu E-Mail server stack. The Webmail and administration tools can be accessed through /mail/
* A Cops E-Book library. It can be accessed through /cops/
* Portainer Container manager. It can be accessed through /portainer/
* A Transmission Torrent downloader. It can be accessed through /transmission/
* A Wake-On-Lan client. It can be accessed through /wakeup/

### A note on domain names or public IPs

A domain name or static public IP is required if you want to be able to connect to your Pi from anywhere using Wireguard.
If you additionally also want to use HTTPS/TLS for your web services, you need certificates. You can only obtain certificates if you access your Pi using proper domain name like ``example.com``, not for a public IP address.

You can obtain a domain name even without a static IP address using a Dynamic DNS provider like DuckDNS, Dynu, No-IP or Securepoint DynDNS. We currently do not automatically install a Dynamic DNS updating tool, so you will need to set up the tool for your chosen provider after the Pi is online.

For the E-Mail server, you need a proper domain for which you have access to the DNS zone configuration. A free dynamic DNS provider will not work for this.

## Further setup

### Port Forwarding

If you want to be able to access your server from outside of your local network, you will need to port forward certain ports of your Pi on your router.

This means that you configure your router to forward any network traffic arriving at port X to your Pi's port X. This can be setup on the configuration website of your router. The details of port forwarding vary between router brands (and even models). Instructions for most routers can be found on https://portforward.com/router.htm.

List of ports to forward:

* 51820: This port should be exposed to allow connecting using Wireguard VPN
* 80: This port can be exposed if you want to expose your web services over HTTP. Required for solving the HTTP certificate challenge
* 443: This port can be exposed if you want to expose your web services over HTTPS/TLS. Required for solving the TLS certificate challenge.
* 22: This port can be exposed if you want to expose access to your Git server using SSH. Note that this is not the port used to connect to your Pi using SSH.
* 25, 110, 143, 465, 587, 993, and 995: These ports are used for inbound and outbound communication for the E-Mail server. More about E-Mail setup can be found below.

We strongly suggest NOT to expose port 2222 (which is used for SSH access to your Pi).

DO NOT expose port 53 (Pihole DNS). If you wish to use Pihole ad-blocking on your mobile devices, connect to your Pi using Wireguard instead.

### Setting up Wireguard VPN

Any clients of the Wireguard VPN need to have ``wireguard`` installed. Wireguard clients for various systems can be found on https://www.wireguard.com/install/.

Additional client configurations can be obtained using the administration WebUI or by invoking the script at ``/etc/wireguard/create-new-conf.sh`` on the server (e.g. using SSH).

<!--TODO: move this to the web UI setup tutorial section-->

<!--### Setting up the E-Mail server-->

<!--E-Mail is a very complex system which needs more setup than just having a web-accessible server running. You need to add a few entries to your DNS zone, namely a MX entry (which tells others that your server is responsible for handling E-Mail for your domain), and several TXT entries for SPF, DKIM, and DMARC (which all work together to prevent spam and forged sender addresses). All the entries you need to add to your DNS zone file can be found on the Mailu Admin UI, at https://<YOURDOMAIN>/mail/admin/ui/domain/details/<YOURDOMAIN>, after you press the "Regenerate keys" button in the top right corner.-->

### Setting up the individual services

We pre-configure the services as much as we can. For services that still require some manual intervention from you, consult the administration WebUI for any additional instructions and links to the configuration manual by the service's creators.

## Building the image yourself

In the following, "Host" will refer to the machine building the image, while "Server" will refer to the machine (Raspberry Pi) you will deploy the image on.

For any questions about pi-gen, read [``pi-gen_README.md``](pi-gen_README.md).

### Dependencies

The host dependencies are listed in [``depends``](depends). As the server image is created by the build process on the host, the server has no dependencies. Any clients using the Wireguard VPN service will need to have ``wireguard`` installed.

The build process automatically checks for and reports missing dependencies. Konwn issue: if you are using Ubuntu 20.04, `qemu-user-static:i386` will not be available by default. you can circumvent this issue by manually installing it. (package [from there](https://launchpad.net/ubuntu/bionic/i386/qemu-user-static/1:2.11+dfsg-1ubuntu7.34))
```
# Download the package
wget http://launchpadlibrarian.net/508305614/qemu-user-static_2.11+dfsg-1ubuntu7.34_i386.deb 
# Add support for 32 bit packages
sudo dpkg --add-architecture i386
# Install the package
sudo dpkg -i qemu-user-static_2.11+dfsg-1ubuntu7.34_i386.deb
```

### Troubleshooting

Make sure that qemu-user-static is installed in the 32-bit version (apt install qemu-user-static:i386) or you are running on a 32-bit host, failing to do so may result in a working build process but images that show some problems (noticeable from SSL certificate errors). To make sure that your resulting image is not corrupted, you can try to run ```curl -sSL https://google.com```.

If building fails to incorporate changes, delete the work folder and retry.

### Build process

The build process executes commands as superuser which have not been heavily tested. Proceed with caution when running such scripts.

It should be possible to run ``sudo build-and-flash.sh`` and let everything be done auomatically. You will be prompted to choose the SD card you want to flash the image on at the start.

In case you only want to build, run ``sudo build.sh``. After building, the finished images can be found in ``deploy/`` and can then manually be flashed to a storage medium of your choice, using e.g. Balena Etcher, Popsicle, or similar image flashing tools.

<!--- Building with Docker is possible, but only tested to work within GitLab CI/CD and not suggested. Refer to pi-gen README for more info about building in Docker.
--->
If you want to ensure complete recompilation, delete the stages you want to produce freshly from the ``work/`` directory.

Note that you will need an internet connection to build.

### Config

A configuration file named ``config`` can be used to provide environment variables to the image building system. Rename or copy [``sample-config``](sample-config) for a good start. The most important configuration options are listed and explained in [``sample-config``](sample-config). Some more options are provided by Pi-Gen and explained in [``pi-gen_README.md``](pi-gen_README.md).

### Advanced configuration

If you want to enable SSH, ``config`` needs to contain a line ``ENABLE_SSH='/path/to/your/ssh_public_key.pub'``. SSH access can then be done using port 2222, for example with the command ``ssh -p 2222 {FIRST_USER_NAME}@10.243.243.1`` while you are connected to it using Wireguard. You can also add a shortcut for this to your ``~/.ssh/config``.

If you want to authorize preexisting Wireguard clients, you want to set `export WG_CLIENTS='/path/to/your/clients-fragments.conf'`. To use a preexisting server keypair, you can specify `export WG_SERVER_KEY='/path/to/server_private.key'`

The build process also creates ``deploy/IMAGENAME-peer.conf``, which is the configuration file for a first Wireguard client of your deployed server. Copy it to ``/etc/wireguard/peer.conf`` and start the Wireguard service using ``sudo systemctl start wg-quick@peer``. You should now have a tunnel established to your deployed server.

# Contributing

If you want to improve the setup of the server itself, contribute here by opening issues, or opening a merge request to merge into develop. Join us on Discord to discuss any issues and ideas: https://discord.gg/CEXrxukXcB

To integrate new services, contribute to the services project. It is integrated as a submodule in this repo under the path `stage7-addons/00-copy-generation-files/files/pier-services`. To use the SSH endpoint to ease the push of changes, you can run `cd stage7-addons/00-copy-generation-files/files/pier-services && git remote set-url --push origin git@gitlab.com:pierhost/pier-services.git`

To improve the Web UI, contribute to the admin project.