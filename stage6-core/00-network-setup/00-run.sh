#!/bin/bash -e
if [[ -z "$DEVICE_IP" ]] || [[ -z "$ROUTER_IP" ]]
then
    log "Device IP or router IP (or both) not specified, skipping static local IP setup"
else
    log "Setting device IP to $DEVICE_IP" 
    log "Setting router IP to $ROUTER_IP"
    install -m 600 files/dhcpd.conf "${ROOTFS_DIR}/etc/dhcpcd.conf"
    sed -i "s|DEVICE_IP|${DEVICE_IP}|g" "${ROOTFS_DIR}/etc/dhcpcd.conf"
    sed -i "s|ROUTER_IP|${ROUTER_IP}|g" "${ROOTFS_DIR}/etc/dhcpcd.conf"
fi
