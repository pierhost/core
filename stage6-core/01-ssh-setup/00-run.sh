#!/bin/bash -e

ssh_key_found=false

user_pub_key_file="$ENABLE_SSH"
if test -f "$user_pub_key_file"
then
    log "Using ${user_pub_key_file} as ssh key"
    pub_key_file="$user_pub_key_file"
    ssh_key_found=true
else
    log "Skipping SSH setup"
fi

if [[ "$ssh_key_found" = "true" ]]; then
    log "A ssh key was specified, setting up ssh"
    #todo: dynamic
    install -D -m 600 -o "1000" "$pub_key_file" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.ssh/authorized_keys"
    # Activate ssh server
    on_chroot << EOF
systemctl enable ssh
EOF
    # Reinforce SSH security
    install -m 600 files/sshd_config "${ROOTFS_DIR}/etc/ssh/sshd_config"    
fi


