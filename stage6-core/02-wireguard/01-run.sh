#!/bin/bash -e

# todo: move this
mkdir -p "${DEPLOY_DIR}"
echo -n "${IMG_FILENAME}-with-services.zip" > "${DEPLOY_DIR}/latest"
###########################################################################################################
#                                          Installation                                                   #
###########################################################################################################
# enabling IP forwarding
install -m 644 files/installation/sysctl.conf "${ROOTFS_DIR}/etc/sysctl.conf"

###########################################################################################################
#                                         Server config                                                   #
###########################################################################################################
#server confing
install -D -m 600 -o 0 files/server/wg0.conf "${ROOTFS_DIR}/etc/wireguard/wg0.conf"
install -m 600 -o 0 files/server/counter_file "${ROOTFS_DIR}/etc/wireguard/counter_file"
install -m 700 -o 0 files/server/create-new-conf.sh "${ROOTFS_DIR}/etc/wireguard/create-new-conf.sh"
install -m 600 files/full-tunnel/peer.conf "${ROOTFS_DIR}/etc/wireguard/peer.conf"
install -m 600 files/full-tunnel/peer-fragment.conf "${ROOTFS_DIR}/etc/wireguard/peer-fragment.conf"
if test -f "$WG_SERVER_KEY"
then
    log "Using existing server's Wireguard key pair" 
    server_private_key=$(cat "$WG_SERVER_KEY") # the keys are base64 encoded, padded with =
    server_public_key=$(wg pubkey <<< "$server_private_key")
    echo "$server_public_key" > "${DEPLOY_DIR}/${IMG_FILENAME}-server_public.key"
else
    log "Creating server's Wireguard key pair" # The server's private key is generated on the build computer
    server_private_key=$(wg genkey) # the keys are base64 encoded, padded with =
    server_public_key=$(wg pubkey <<< "$server_private_key")
    echo "$server_public_key" > "${DEPLOY_DIR}/${IMG_FILENAME}-server_public.key"
fi

if [[ -z "$ROUTER_IP" ]]
then
    WIREGUARD_DNS="192.168.0.1"
    log "No Router IP specified, assuming 192.168.0.1"
else
    WIREGUARD_DNS="${ROUTER_IP}"
fi

sed -i "s&server_private_key&${server_private_key}&g" "${ROOTFS_DIR}/etc/wireguard/wg0.conf"
sed -i "s&replace_with_router_ip&${WIREGUARD_DNS}&g" "${ROOTFS_DIR}/etc/wireguard/wg0.conf"
sed -i "s&server_ip&${DEVICE_DNS}&g"                 "${ROOTFS_DIR}/etc/wireguard/peer.conf"
sed -i "s&server_public_key&${server_public_key}&g" "${ROOTFS_DIR}/etc/wireguard/peer.conf"
###########################################################################################################
#                                         Client config                                                   #
###########################################################################################################
if test -f "$WG_CLIENTS"
then
# Set the needed variables
    log "Using existing client configurations" 
    user_id=$(grep '\[Peer\]' "$WG_CLIENTS" | wc -l | awk '{ print 1+$1 }')
    # Update the server's conf
    cat "$WG_CLIENTS" >> "${ROOTFS_DIR}/etc/wireguard/wg0.conf"
else
    log "Creating new client for Wireguard"
    # Set the needed variables
    user_id=$(awk '{ print 1+$1 }' "${ROOTFS_DIR}/etc/wireguard/counter_file")
    client_private_key=$(wg genkey) # the keys are base64 encoded, padded with =
    client_public_key=$(wg pubkey <<< "$client_private_key")
    # Generate the client configuration
    install -m 600 files/full-tunnel/peer.conf            "${DEPLOY_DIR}/${IMG_FILENAME}-peer.conf"
    sed -i "s&client_private_key&${client_private_key}&g" "${DEPLOY_DIR}/${IMG_FILENAME}-peer.conf"
    sed -i "s&server_public_key&${server_public_key}&g"   "${DEPLOY_DIR}/${IMG_FILENAME}-peer.conf"
    sed -i "s&user_id&${user_id}&g"                       "${DEPLOY_DIR}/${IMG_FILENAME}-peer.conf"
    sed -i "s&server_ip&${DEVICE_DNS}&g"                   "${DEPLOY_DIR}/${IMG_FILENAME}-peer.conf"
    # Update the server's conf
    cat files/full-tunnel/peer-fragment.conf | \
    sed "s&client_public_key&${client_public_key}&g" | \
    sed "s&user_id&${user_id}&g" >> "${ROOTFS_DIR}/etc/wireguard/wg0.conf"
fi

printf "%s" "$user_id" > "${ROOTFS_DIR}/etc/wireguard/counter_file"

