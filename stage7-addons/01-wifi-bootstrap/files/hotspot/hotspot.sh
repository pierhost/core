#!/bin/bash -e

function usage() {
    echo "usage: $0 [start|restart|stop]"
    echo "* start: start the hotspot"
    echo "* restart: stop and restart the hotspot"
    echo "* stop: stops the hotspot"
    echo "* reapply: reapplies non-hotspot dhcpcd.conf -- requires hotspot to be off"
    exit 1
}

cmd=${1:-none}
case $cmd in
    start) action="start";;
    restart) action="restart";;
    stop) action="stop";;
    autostart) action="autostart";;
    reapply) action="reapply";;
    *) usage;;
esac

function isRunning() {
    systemctl is-active --quiet hostapd
    hostapdStatus=$?
    systemctl is-active --quiet dnsmasq
    dnsmasqStatus=$?

    if [ $hostapdStatus -eq 0 ] && [ $dnsmasqStatus -eq 0 ]
    then
        return 0
    else
        return 1
    fi
}

function ethernetConnected() {
    ethtool "$1" | grep -q "Link detected: yes"
}

function wifiConnected() {
    iw "$1" link | grep -q "SSID"
}

function flushIPsAndRestartDhcpcd() {
    systemctl stop dhcpcd.service
    for dev in wlan0 eth0 lo; do
        ip addr flush dev $dev
    done
    systemctl start dhcpcd.service
    systemctl restart networking.service
}

function stopHotspot() {
    if ! isRunning
    then
        echo "Hotspot is not running"
        exit 1
    fi

    systemctl disable --now --quiet hostapd
    cp hostapd.old /etc/default/hostapd
    systemctl disable --now --quiet dnsmasq
    cp dnsmasq.conf.old /etc/dnsmasq.conf
    cp dhcpcd.conf.old /etc/dhcpcd.conf

    # finalize networking changes by restarting network stack
    flushIPsAndRestartDhcpcd

    # restart pihole if it was stopped by hotspot
    if [[ -e piholeStopped ]]
    then
        docker start "pihole"
        rm piholeStopped
    fi
}

function startHotspot() {
    if isRunning
    then
        echo "Hotspot is already running"
        exit 1
    fi

    # shut down pihole if it is running
    if docker ps -f "name=pihole" --format "{{.Names}}" | grep -q "pihole"
    then
        touch piholeStopped
        docker stop "pihole"
    fi

    # remove rfkill if present
    rfkill unblock wlan

    # set up dhcpcd
    cp /etc/dhcpcd.conf dhcpcd.conf.old
    cp dhcpcd.conf /etc/dhcpcd.conf

    # set up hostapd
    cp /etc/default/hostapd hostapd.old
    echo "DAEMON_CONF=\"$(dirname "$(readlink -f "$0")")/hostapd.conf\"" > /etc/default/hostapd
    
    # set up dnsmasq
    cp /etc/dnsmasq.conf dnsmasq.conf.old
    cp dnsmasq.conf /etc/dnsmasq.conf
    systemctl enable --now --quiet dnsmasq

    # finalize by restarting the networking
    flushIPsAndRestartDhcpcd
    systemctl enable --now --quiet hostapd
}

function autoStart() {
    if [[ -e firstStartupDone ]]
    then
        # not first startup
        # only start hotspot if there is no connectivity
        if ! wifiConnected wlan0 && ! ethernetConnected eth0
        then
            startHotspot
        fi
    else
        # first startup
        # start hotspot no matter what
        startHotspot
        touch firstStartupDone
    fi
}

function reapplyDHCPCD() {
    if isRunning
    then
        echo "cannot reapply non-hotspot dhcpcd.conf while hotspot is running"
        exit 1
    fi

    cp dhcpcd.conf.nonhotspot /etc/dhcpcd.conf
    flushIPsAndRestartDhcpcd
}

function action() {
    [ "$action" == "stop" ] && stopHotspot;
    [ "$action" == "restart" ] && stopHotspot && startHotspot;
    [ "$action" == "start" ] && startHotspot;
    [ "$action" == "autostart" ] && autoStart;
    [ "$action" == "reapply" ] && reapplyDHCPCD;
    return 0
}

pushd "$(dirname "$(readlink -f "$0")")"
action
popd